import copy
from time import sleep
from selenium.webdriver.common.by import By

def test_get_url(driver):
    driver.get('https://www.chitai-gorod.ru')
    search = driver.find_element(By.XPATH,'/html/body/div[2]/header/div/div[2]/div/form/input')
    search.send_keys('тестирование')
    button_search = driver.find_element(By.XPATH, '/html/body/div[2]/header/div/div[2]/div/form/button')
    button_search.click()
    end_range = 8
    print('\n')


    #  Список сожержащий информацию карточек (книг) на странице "Результаты поиска"
    def parsing_product_card(start, stop,end_range):
        book_card = list()
        for i in range(end_range):
            for item in driver.find_elements(By.XPATH,"/html/body/div[2]/main/div[1]/div/div[2]/div[5]/div[" + str(i+1) + "]"):
                book_card.append(item.text.split('\n')[start:stop])
        return book_card
    # Редактирование списка информации карточек:
    # 1. Удалив ствойства вида : "Новинка", "Распродажа", "Бестселлер".
    # 2. Манипулировать количеством нужной информации для последующего сравнения со страницей "Корзина".
    def edit_list_product_card(start, stop):
        book_card = parsing_product_card(0, 10, end_range)
        for i in range(end_range):
            if book_card[i][0] in ["Новинка", "Распродажа", "Бестселлер"]:
                del book_card[i][0]
        edit_info_list = [book_card[i][start:stop] for i in range(end_range-1)]
        return edit_info_list

    book_info = edit_list_product_card(0, 2)
    book_price = edit_list_product_card(2, 3)
    print(book_info)
    # print(book_price)

    # Покупка книг
    def buy_book_click():
        for i in range(1,end_range):
            driver.find_element(By.XPATH, '/html/body/div[2]/main/div[1]/div/div[2]/div[5]/div[' + str(i) + ']/div/div[3]/button').click()
    # Переход на страницу "Корзина"
    def go_to_basket_page():
        driver.find_element(By.XPATH, '/html/body/div[2]/header/div/div[2]/div/div[2]/div/a[1]').click()

    buy_book_click()
    go_to_basket_page()
    # Переход в корзину

    def book_card_info_in_basket(start, stop, end_range):
        book_card_in_basket = list()
        for i in range(end_range):
            for item in driver.find_elements(By.XPATH,"/html/body/div[2]/main/div/div[2]/div/div[1]/div[3]/div["+str(i+1)+"]"):
                book_card_in_basket.append(item.text.split('\n')[start:stop])
        return book_card_in_basket

    def book_count(end_range):
        count = list()
        for i in range(1, end_range):
            for item in driver.find_element(By.XPATH,"/html/body/div[2]/main/div/div[2]/div/div[1]/div[3]/div["+str(i)+"]/div[1]/div[2]/div[1]/div[1]/input").get_attribute("value"):
                count.append(int(item))
        return sum(count)

    book_info_basket = book_card_info_in_basket(0,2,end_range)
    book_prices_in_basket = book_card_info_in_basket(6,7,end_range)

    print(book_info_basket)
    # print(book_prices_in_basket)

    # Сумма всех книг.
    def total_sum(book_prices_in_basket):
        sum = 0
        for i in range(end_range-1):
            price = int(book_prices_in_basket[i][0][0:-2])
            sum = sum + price
        return sum
    # print(total_sum(book_prices_in_basket))

    book_count_basket= int(driver.find_element(By.XPATH,"/html/body/div[2]/header/div/div[2]/div/div[2]/div/a[1]/span[2]").text)
    total_sum_in_basket = int(driver.find_element(By.XPATH,"/html/body/div[2]/main/div/div[2]/div/div[2]/div/div/div[2]/div[2]/span").text)
    # print(book_count_basket)
    # print(total_sum_in_basket)
    # assert book_info == book_info_basket, "Название книг и их авторы совпадают"
    # assert book_price == book_prices_in_basket, "Стоимость книг совпадают"
    # assert book_count(end_range) == book_count_basket, "Количество книг совпадают"
    # assert total_sum(book_prices_in_basket) == total_sum_in_basket, "Общая сумма совпадает"

    # def remove_book(number, end_range):
    #     if number in range(1, end_range):
    #         driver.find_element(By.XPATH,"/html/body/div[2]/main/div/div[2]/div/div[1]/div[3]/div["+str(number)+"]/div/div[3]/div[2]").click()
    #         book_info_basket.pop(number - 1)
    #         book_prices_in_basket.pop(number - 1)
    #         end_range = end_range - 1
    #         return end_range
    # #  book_prices_in_basket.pop(number)
    # end_range = remove_book(2,end_range)
    # print(end_range)
    # end_range = remove_book(1, end_range)
    # print(end_range)
    # print('\n')
    # sleep(2)
    # book_count_basket = int(driver.find_element(By.XPATH,"/html/body/div[2]/header/div/div[2]/div/div[2]/div/a[1]/span[2]").text)
    # total_sum_in_basket = int(driver.find_element(By.XPATH,"/html/body/div[2]/main/div/div[2]/div/div[2]/div/div/div[2]/div[2]/span").text)
    # # print(book_count_basket)
    # # print(book_count(end_range))
    # # print(total_sum_in_basket)
    # # print(total_sum(book_prices_in_basket))
    # # print(book_info_basket)
    # # print(book_prices_in_basket)
    #
    # assert book_count(end_range) == book_count_basket, "Количество книг совпадают"
    # assert total_sum(book_prices_in_basket) == total_sum_in_basket, "Общая сумма совпадает"