from time import sleep

from allure_commons.types import Severity
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
import allure


@allure.title('End-to-End test')
def test_get_url(driver):

    # Количество книг для покупки
    book_id = 3
    wait = WebDriverWait(driver, 100)
    # Открыть страницу

    def open():
        with allure.step('Открываем страницу "https://www.chitai-gorod.ru"'):
            driver.get('https://www.chitai-gorod.ru')

    # Поиск по запросу "тестирование"
    def request_search():
        with allure.step("Вводим в поле поиска сайта слово 'тестирование'"):
            search = driver.find_element(By.XPATH, '/html/body/div[2]/header/div/div[2]/div/form/input')
            search.send_keys('тестирование')
        with allure.step("Нажимаем кнопку поиска"):
            button_search = driver.find_element(By.XPATH, '/html/body/div[2]/header/div/div[2]/div/form/button')
            button_search.click()

    # Разбор карточки товара (книги) на странице "Результаты поиска"
    def parsing_product_card(start, stop, book_id):
        with allure.step("Парсим  нужную информацию карточки товара на странице 'Результаты поиска'"):
            book_card = list()
            for i in range(book_id):
                for item in wait.until(EC.presence_of_all_elements_located((By.XPATH,"/html/body/div[2]/main/div[1]/div/div[2]/div[5]/div[" + str(i + 1) + "]"))):
                    book_card.append(item.text.split('\n')[start:stop])
            return book_card

    # Редактирование списка карточек товаров (книги):
    # 1. Удалив ствойства вида : "Новинка", "Распродажа", "Бестселлер".
    # 2. Манипулировать количеством нужной информации для последующего сравнения со страницей "Корзина".
    def edit_list_product_card(start, stop):
        book_card = parsing_product_card(0, 10, book_id)
        for i in range(book_id):
            if book_card[i][0] in ["Новинка"]:
                del book_card[i][0]
        edit_info_list = [book_card[i][start:stop] for i in range(book_id)]
        return edit_info_list

    # Добавить книгу в корзину
    def buy_book():
        with allure.step("Покупаем выбранное количество книг"):
            for i in range(1, book_id+1):
                wait.until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/main/div[1]/div/div[2]/div[5]/div[' + str(i) + ']/div/div[3]/button'))).click()

    # Переход в корзину
    def open_basket():
        with allure.step("Открываем корзину"):
            wait.until(EC.element_to_be_clickable((By.XPATH,'/html/body/div[2]/header/div/div[2]/div/div[2]/div/a[1]'))).click()

    # Разбор карточки товара (книги) на странице "Корзина"
    def book_card_info_in_basket(start, stop, book_id):
        with allure.step("Парсим информацию карточки товара на странице 'Корзина'"):
            title_book_author_basket = list()
            for i in range(book_id):
                for item in wait.until(EC.presence_of_all_elements_located((By.XPATH,"/html/body/div[2]/main/div/div[2]/div/div[1]/div[3]/div["+str(i+1)+"]"))):
                    title_book_author_basket.append(item.text.split('\n')[start:stop])
            return title_book_author_basket

    # Количество добавленых книг через подсчет счетчика товара
    def book_count(book_id):
        with allure.step("Подсчитываем книги по счетчику количества товара"):
            title_book_author_basket = list()
            for i in range(1, book_id+1):
                for item in wait.until(EC.presence_of_element_located((By.XPATH,"/html/body/div[2]/main/div/div[2]/div/div[1]/div[3]/div["+str(i)+"]/div[1]/div[2]/div[1]/div[1]/input"))).get_attribute("value"):
                    title_book_author_basket.append(int(item))
            return sum(title_book_author_basket)

    # Суммирование цен из карточки товара для подсчета общей стоимостьи книг
    def total_sum(book_price_in_basket):
        with allure.step("Подсчитываем общую сумму всех книг по информации цен из карточек товара"):
            total = 0
            for i in range(book_id):
                price = int(book_price_in_basket[i][0][0:-2])
                total = total + price
            return total

    # Количество добавленных книг в корзине
    def book_count_basket():
        count = int(wait.until(EC.presence_of_element_located(
            (By.XPATH, "/html/body/div[2]/header/div/div[2]/div/div[2]/div/a[1]/span[2]"))).text)
        return count

    # Общая стоимость книг в корзине
    def total_sum_in_basket():
        total = int(wait.until(EC.presence_of_element_located(
            (By.XPATH, "/html/body/div[2]/main/div/div[2]/div/div[2]/div/div/div[2]/div[2]/span"))).text)
        return total

    # Удаление выбранной книги (номер) из корзины
    def remove_book(number: int, book_id):
        with allure.step("Удаляем нужную книгу (номер)"):
            if number in range(book_id + 1):
                wait.until(EC.element_to_be_clickable(
                    (By.XPATH,"/html/body/div[2]/main/div/div[2]/div/div[1]/div[3]/div[" + str(number) + "]/div/div[3]/div[2]"))).click()
                book_info_basket.pop(number - 1)
                book_prices_in_basket.pop(number - 1)
                return book_id - 1

    open()
    request_search()
    book_info = edit_list_product_card(0, 2)
    book_price = edit_list_product_card(2, 3)
    buy_book()
    open_basket()
    book_info_basket = book_card_info_in_basket(0,2,book_id)
    book_prices_in_basket = book_card_info_in_basket(6,7,book_id)

    with allure.step("Корректность соотвествия выбранной книги по автору и названию между страницыми 'Результаты поиска' и 'Корзина'"):
        assert book_info == book_info_basket, "Название книг и их авторы совпадают"
    with allure.step("Корректность стоимости книг между страницыми 'Результаты поиска' и 'Корзина'"):
        assert book_price == book_prices_in_basket, "Стоимость книг совпадают"
    with allure.step("Корректность количества книг по счетчику товара и счетчиком на странице 'Корзина'"):
        assert book_count(book_id) == book_count_basket(), "Количество книг совпадают"
    with allure.step("Корректность соотвествия общей суммы книг с полем 'Общая сумма товара' на странице 'Корзина'"):
        assert total_sum(book_prices_in_basket) == total_sum_in_basket(), "Общая сумма совпадает"
    book_id = remove_book(1, book_id)
    sleep(2)
    with allure.step("Корректность количества книг по счетчику товара и счетчиком на странице 'Корзина'"):
        assert book_count(book_id) == book_count_basket(), "Количество книг совпадают"
    with allure.step("Корректность соотвествия общей суммы книг с полем 'Общая сумма товара' на странице 'Корзина'"):
        assert total_sum(book_prices_in_basket) == total_sum_in_basket(), "Общая сумма совпадает"
