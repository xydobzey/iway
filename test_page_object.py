from time import sleep

from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class BasePage():
    def __init__(self, driver, url, time_out=10):
        self.driver = driver
        self.wait = WebDriverWait(driver, 100)
        self.url = url
        self.driver.implicitly_wait(time_out)
        self.book_id = 3
        # self.book_info_basket = []
        # self.book_prices_in_basket = []

    def open(self):
        self.driver.get(self.url)

    def request_search(self):
        search = self.driver.find_element(By.XPATH, '/html/body/div[2]/header/div/div[2]/div/form/input')
        search.send_keys('тестирование')
        button_search = self.driver.find_element(By.XPATH, '/html/body/div[2]/header/div/div[2]/div/form/button')
        button_search.click()

    def parsing_product_card(self, start: int, stop: int):
        book_card = list()
        for i in range(self.book_id):
            for item in self.wait.until(EC.presence_of_all_elements_located((By.XPATH, "/html/body/div[2]/main/div[1]/div/div[2]/div[5]/div[" + str(i + 1) + "]"))):
                book_card.append(item.text.split('\n')[start:stop])
        return book_card

    def edit_list_product_card(self, start: int, stop: int):
        book_card = self.parsing_product_card(0, 10)
        for i in range(self.book_id):
            if book_card[i][0] in ["Новинка"]:
                del book_card[i][0]
        edit_info_list = [book_card[i][start:stop] for i in range(self.book_id)]
        return edit_info_list

        # Купить первые 3 книги
    def buy_book(self):
        for i in range(1, self.book_id + 1):
            self.wait.until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/main/div[1]/div/div[2]/div[5]/div[' + str(i) + ']/div/div[3]/button'))).click()

    def open_basket(self):
        button_basket = self.wait.until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/header/div/div[2]/div/div[2]/div/a[1]')))
        button_basket.click()

    def book_card_info_in_basket(self, start: int, stop: int):
        title_book_author_basket = list()
        for i in range(self.book_id):
            for item in self.wait.until(EC.presence_of_all_elements_located(
                    (By.XPATH, "/html/body/div[2]/main/div/div[2]/div/div[1]/div[3]/div[" + str(i + 1) + "]"))):
                title_book_author_basket.append(item.text.split('\n')[start:stop])
        return title_book_author_basket

    def book_count(self):
        title_book_author_basket = list()
        for i in range(1, self.book_id + 1):
            for item in self.wait.until(EC.presence_of_element_located((By.XPATH,"/html/body/div[2]/main/div/div[2]/div/div[1]/div[3]/div[" + str(i) + "]/div[1]/div[2]/div[1]/div[1]/input"))).get_attribute("value"):
                title_book_author_basket.append(int(item))
        return sum(title_book_author_basket)


    def total_sum(self):
        book_price_in_basket = self.book_card_info_in_basket(6, 7)
        total = 0
        for i in range(self.book_id):
            price = int(book_price_in_basket[i][0][0:-2])
            total = total + price
        return total

    def book_count_basket(self):
        count = int(self.wait.until(EC.presence_of_element_located((By.XPATH, "/html/body/div[2]/header/div/div[2]/div/div[2]/div/a[1]/span[2]"))).text)
        return count

    def total_sum_in_basket(self):
        total_sum_in_basket = int(self.wait.until(EC.presence_of_element_located((By.XPATH, "/html/body/div[2]/main/div/div[2]/div/div[2]/div/div/div[2]/div[2]/span"))).text)
        return total_sum_in_basket


    def remove_book(self, number: int):
        book_info_basket = self.book_card_info_in_basket(0,2)
        book_prices_in_basket = self.book_card_info_in_basket(6,7)
        if number in range(self.book_id):
            self.wait.until(EC.element_to_be_clickable((By.XPATH,"/html/body/div[2]/main/div/div[2]/div/div[1]/div[3]/div["+str(number)+"]/div/div[3]/div[2]"))).click()
            self.book_id = self. book_id






def test_01(driver):
    link = "https://www.chitai-gorod.ru"
    page = BasePage(driver, link)
    page.open()
    page.request_search()
    print('\n')
    print(page.edit_list_product_card(0,2))
    print(page.edit_list_product_card(2,3))
    page.buy_book()
    page.open_basket()
    print(page.book_card_info_in_basket(0,2))
    print(page.book_card_info_in_basket(6,7))
    print(page.book_count())
    # print(page.total_sum())
    print(page.book_count_basket())
    # print(page.total_sum_in_basket())
    # assert book_info == book_info_basket, "Название книг и их авторы совпадают"
    # assert book_price == book_prices_in_basket, "Стоимость книг совпадают"
    # assert book_count(book_id) == book_count_basket, "Количество книг совпадают"
    # assert total_sum(book_prices_in_basket) == total_sum_in_basket, "Общая сумма совпадает"
    page.remove_book(1)
    print(page.total_sum())
    sleep(2)

    # print(page.book_count())
    # print(page.book_card_info_in_basket(0,2))
    # print(page.book_card_info_in_basket(6,7))
    print(page.total_sum_in_basket())