import pytest
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

@pytest.fixture()
def driver():

    options = Options()
    # options.add_argument("--headless")
    options.add_argument("--start-maximized")
    options.add_argument("--ignore-certificate-errors")
    options.add_argument("--disable-popup-blocking")
    options.add_argument("--incognito")
    driver = webdriver.Chrome(options=options, executable_path='D://selenium//chromedriver.exe')
    driver.implicitly_wait(100)
    # driver.get('https://www.chitai-gorod.ru')
    yield driver
    driver.quit()
