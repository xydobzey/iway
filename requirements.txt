selenium==3.141.0
allure-pytest==2.8.0
pytest==5.1.1
pytest-xdist==1.29.0
requests==2.22.0
